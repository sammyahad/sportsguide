from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
@app.route('/web')
@app.route('/web.html')
def home():
    return render_template('web.html')

@app.route('/about')
@app.route('/about.html')
def about():
    return render_template('about.html', title="about")
    
    

    
    


if __name__ == '__main__':
    app.run(port=8080, host='0.0.0.0', debug=True)